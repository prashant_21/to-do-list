import React from 'react';
import { Link } from 'react-router-dom';

function header() {
    return (
        <header style={headerStyle}>
            <h1>To-Do List</h1>
            <Link style={linkStyle} to='/'>Home</Link> | <Link style={linkStyle} to='/about'>About</Link>
        </header>
    )
}

const headerStyle = {
    background: '#333',
    color: '#fff',
    fontSize: '1.8rem',
    textAlign: 'center',
    padding: '1rem'
}

const linkStyle = {
    color: '#fff',
    textDecoration: 'none'
}

export default header;