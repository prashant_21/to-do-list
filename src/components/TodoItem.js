import React, { Component } from 'react';
import PropTypes from 'prop-types';


export class TodoItem extends Component {
    getStyle = () => {
        return {
            background: '#f4f4f4',
            padding: '1rem',
            borderBottom: '0.1rem #ccc dotted',
            textDecoration: this.props.todo.completed ?
                'line-through' : 'none',
            fontSize: '1.2rem'
        }
    }

    render() {
        const { id, title } = this.props.todo;
        return (
            <div style={this.getStyle()}>
                <p>
                    <input type='checkbox' onChange={this.props.markComplete.bind(this, id)} /> {' '}
                    {title}
                    <button onClick={this.props.delete.bind(this, id)} style={btnStyle}>X</button>
                </p>
            </div>
        )
    }
}

// proptypes
TodoItem.propTypes = {
    todo: PropTypes.object.isRequired,
    markComplete: PropTypes.func.isRequired,
    delete: PropTypes.func.isRequired
}

const btnStyle = {
    background: '#ff0000',
    color: '#fff',
    border: 'none',
    padding: '0.25rem 0.5rem',
    borderRadius: '50%',
    cursor: 'pointer',
    float: 'right',
}

export default TodoItem
